package com.example.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class CountryInfo extends AppCompatActivity
{

    private Country country;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_info);

        // get intent which called current activity
        Intent intent = getIntent();
        // name of the country to edit
        final String countryName = intent.getStringExtra("country");
        // actual country to edit
        country = CountryList.getCountry(countryName);

        // title
        TextView countryNameView = findViewById(R.id.countryNameTextView);
        countryNameView.setText(countryName);

        // flag
        ImageView flagView = findViewById(R.id.flagImageView);
        flagView.setImageResource(this.getResources().getIdentifier(country.getmImgFile(), "drawable", this.getPackageName()));

        // capital
        final TextView capitalView = findViewById(R.id.capitaleView);
        capitalView.setText(country.getmCapital());

        // language
        final TextView languageView = findViewById(R.id.langueView);
        languageView.setText(country.getmLanguage());

        // currency
        final TextView currencyView = findViewById(R.id.monnaieView);
        currencyView.setText(country.getmCurrency());

        // population
        final TextView populationView = findViewById(R.id.populationView);
        populationView.setText(String.valueOf(country.getmPopulation()));

        // area
        final TextView areaView = findViewById(R.id.superficieView);
        areaView.setText(String.valueOf(country.getmArea()));

        // save button
        Button saveButton = findViewById(R.id.saveButton);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                country.setmArea(Integer.parseInt(areaView.getText().toString()));
                country.setmPopulation(Integer.parseInt(populationView.getText().toString()));
                country.setmCapital(capitalView.getText().toString());
                country.setmLanguage(languageView.getText().toString());
                country.setmCurrency(currencyView.getText().toString());

                CountryList.setCountry(countryName, country);
                Toast.makeText(CountryInfo.this, "Sauvegardé!", Toast.LENGTH_SHORT).show();
            }
        });


    }
}